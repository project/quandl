<?php

/**
 * @file
 *   Views query plugin for Quandl queries.
 */



/**
 * Views query plugin for Quandl queries.
 */
class quandl_views_plugin_query extends views_plugin_query {

  public $options = array();


    /**
    * Builds the necessary info to execute the query.
    */
    function build(&$view) {
        
        // Add the Quandl Object to the View
        if (!isset($this->quandl_object)) {
            $this->quandl_object = quandl_object();
        }
        
        
        $view->init_pager();
        
        // Let the pager modify the query to add limits.
        $this->pager->query();
        
        $view->build_info['query'] = $this->query();
        $view->build_info['count_query'] = 'count(' . $view->build_info['query'] . ')';
        $view->build_info['query_args'] = array();
        
        
    }




    function query($get_count = FALSE) { 
        
    }

    function execute(&$view) {

    // Setup the execution timer
    $start = microtime(TRUE);
    $view->execute_time = NULL;
    
    
    if ($this->options['use_args'] && sizeof($view->args) >= 2) {
        $symbol  = $view->args[0] . "/" . $view->args[1] ;
    } 
    
    else {
        $symbol  = $this->options['quandl_code'] . "/" . $this->options['dataset_code'] ;
    }
    
    $qoptions = array();  


    if ($this->options['trim_start'] != '') {
        $qoptions['trim_start'] = $this->options['trim_start'];
    }    

    if ($this->options['trim_end'] != '') {
        $qoptions['trim_end'] = $this->options['trim_end'];
    } 

    $qoptions['sort_order']   = $this->options['sort_order'];
    $qoptions['exclude_data'] = $this->options['exclude_data'];
    $qoptions['collapse'] = $this->options['collapse'];
    
    if ($this->options['exclude_data'] == 'true') {
        $qoptions['exclude_data'] = $this->options['exclude_data'];
    } 
        
   

    
    $this->quandl_data = cached_quandl_symbol_query($this->quandl_object, $symbol, $qoptions);    
    $data_array = $this->quandl_data->data;
    $columns_array = $this->quandl_data->column_names;
    
    $this->pager->total_items = sizeof($data_array);
    $this->pager->update_page_info();
    
    
    $total = $this->pager->total_items;
    $per_page = $this->pager->options['items_per_page'];
    $offset = $this->pager->options['offset'];
    
    // Paging
    if ($per_page > 0) {
        // Use Paging
        $current_page = $this->pager->current_page;
        $start =  ($current_page * $per_page) + $offset;
        $end = $start + $per_page - 1;
        
        
        if ($end > $total) {
            $end = $start + ($total - $start) - 1;
        }
        
    } 
    else {
        // Show all items
        $start = 0 + $offset;
        $end = $total - 1;
    }
      
    
    for ($r = $start; $r <= $end; $r++) {
        
        $data = $data_array[$r];
        
        $row = new stdClass;
        
        $row->Date = $data[0];
    
            
        foreach ($columns_array as $colkey => $column) {
          if ($colkey == 0) continue;
          
            $row->$column = $data[$colkey];
          
        }
     
      $view->result[] = $row;
    }
    
    // Total Rows returned
    $view->total_rows = sizeof($this->quandl_data->data);
    
    // Finish Execution time
    $view->execute_time = microtime(TRUE) - $start;
    
    
  }

  function add_field($table, $field, $alias = '', $params = array()) {
    $alias = $field;
    
    // Add field info array.
    if (empty($this->fields[$field])) {
      $this->fields[$field] = array(
      'field' => $field,
      'table' => $table,
      'alias' => $alias,
      ) + $params;
    }

    return $field;
  }


    function option_definition() {
        $options = parent::option_definition();
        
        $options['sort_order'] = array(
          'default' => 'desc',
          'translatable' => FALSE,
        );
        $options['quandl_code'] = array(
          'default' => 'WIKI',
          'translatable' => FALSE,
        );
        $options['dataset_code'] = array(
          'default' => 'AAPL',
          'translatable' => FALSE,
        );

        $options['trim_start'] = array(
          'default' => 'today-5 years',
          'translatable' => FALSE,
        );

        $options['trim_end'] = array(
          'default' => 'today',
          'translatable' => FALSE,
        );
       
        $options['collapse'] = array(
          'default' => 'monthly',
          'translatable' => FALSE,
        );
        
        $options['exclude_data'] = array(
          'default' => 'false',
          'translatable' => FALSE,
        );
        
        $options['use_args'] = array(
          'default' => FALSE,
          'translatable' => FALSE,
        );

        return $options;
    }

  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);


    $form['quandl_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Quandl Code'),
      '#description' => t('The base Quandl Code (or database) to query.'),
      '#default_value' => $this->options['quandl_code'],
    );
    
    $form['dataset_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Dataset Code'),
      '#description' => t('The Code of the dataset (table) to query from the database'),
      '#default_value' => $this->options['dataset_code'],
    );


    $form['use_args'] = array(
        '#type' => 'select',
        '#title' => t('Use URL arguments for codes'),
        '#options' => array(
          TRUE => t('Yes'),
          FALSE => t('No'),
        ),
        '#description' => t('Pull the Quandl codes from the URL. Source code will be %1 and data set code will be %2'),
        '#default_value' => $this->options['use_args'],
    );

    $form['trim_start'] = array(
      '#type' => 'textfield',
      '#title' => t('Trim Start Date'),
      '#description' => t('The Start Date for the results. You can use \'YYYY-MM-DD\' or any string that can be used with php strtotime.'),
      '#default_value' => $this->options['trim_start'],
    );
    
    $form['trim_end'] = array(
      '#type' => 'textfield',
      '#title' => t('Trim End Date'),
      '#description' => t('The End Date for the results. You can use \'YYYY-MM-DD\' or any string that can be used with php strtotime.'),

      '#default_value' => $this->options['trim_end'],
    );






    $form['collapse'] = array(
        '#type' => 'select',
        '#title' => t('Frequency'),
        '#options' => array(
          'none' => t('Default'),
          'daily' => t('Daily'),
          'weekly' => t('Weekly'),
          'monthly' => t('Monthly'),
          'quarterly' => t('Quarterly'),
          'annual' => t('Annual'),
        ),
        '#description' => t('When you change the frequency of a dataset, Quandl returns the last observation for the given period. So, if you collapse a daily dataset to monthly, you will get a sample of the original dataset where the observation for each month is the last data point available for that month.'),
        '#default_value' => $this->options['collapse'],
    );


    
     $form['sort_order'] = array(
      '#type' => 'select',
      '#title' => t('Sort Order'),
      '#options' => array(
          'asc' => t('Ascending'),
          'desc' => t('Descending'),
       ),
      '#description' => t('The sort order of the returned data. Default is Descending.'),
      '#default_value' => $this->options['sort_order'],
    );
 
    $form['exclude_data'] = array(
        '#type' => 'select',
        '#title' => t('Exclude Data'),
        '#options' => array(
          'true' => t('Yes'),
          'false' => t('No'),
        ),
        '#description' => t('Perform the query without returning any rows'),
        '#default_value' => $this->options['exclude_data'],
    );

 
 
 
    
  }
  
   /**
   * Special submit handling.
   */
  function options_submit(&$form, &$form_state) {
    $this->options = $form_state['values']['query']['options'];
  }
}
