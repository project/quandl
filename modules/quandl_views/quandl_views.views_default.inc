<?php

/**
 * @file quandl_views.views_default.inc
 *
 * Definition of quandl_views_handler_field_date.
 */

/**
 * Implementation of hook_views_default_views().
 */
function quandl_views_views_default_views() {


$view = new view();
$view->name = 'quandl_table';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'quandl_views';
$view->human_name = 'Quandl Stock Table';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'AAPL OHLC Example';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['collapse'] = 'daily';
$handler->display->display_options['query']['options']['use_args'] = '1';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '15';
$handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
$handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '5, 10, 20, 40, 60, 100';
$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = TRUE;
$handler->display->display_options['style_plugin'] = 'table';
/* Header: Quandl: Metadata */
$handler->display->display_options['header']['quandl_summary_area']['id'] = 'quandl_summary_area';
$handler->display->display_options['header']['quandl_summary_area']['table'] = 'quandl_views';
$handler->display->display_options['header']['quandl_summary_area']['field'] = 'quandl_summary_area';
$handler->display->display_options['header']['quandl_summary_area']['content'] = '<strong>@name</strong><br />
<small>@updated_at</small>';
/* Footer: Global: Result summary */
$handler->display->display_options['footer']['result']['id'] = 'result';
$handler->display->display_options['footer']['result']['table'] = 'views';
$handler->display->display_options['footer']['result']['field'] = 'result';
$handler->display->display_options['footer']['result']['content'] = '<br /><br />Displaying @start - @end of @total rows<br />
@per_page per page | current page @current_page | @current_record_count page record count | @page_count pages<br />';
/* Field: Global: View result counter */
$handler->display->display_options['fields']['counter']['id'] = 'counter';
$handler->display->display_options['fields']['counter']['table'] = 'views';
$handler->display->display_options['fields']['counter']['field'] = 'counter';
$handler->display->display_options['fields']['counter']['label'] = 'Count';
$handler->display->display_options['fields']['counter']['counter_start'] = '1';
/* Field: Quandl: Date */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'quandl_views';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['quandl_result_key'] = 'Date';
$handler->display->display_options['fields']['title']['date_format'] = 'custom';
$handler->display->display_options['fields']['title']['custom_date_format'] = 'j/m/Y';
$handler->display->display_options['fields']['title']['second_date_format'] = 'long';
/* Field: Quandl: Numeric */
$handler->display->display_options['fields']['quandl_numeric_1']['id'] = 'quandl_numeric_1';
$handler->display->display_options['fields']['quandl_numeric_1']['table'] = 'quandl_views';
$handler->display->display_options['fields']['quandl_numeric_1']['field'] = 'quandl_numeric';
$handler->display->display_options['fields']['quandl_numeric_1']['label'] = 'Open';
$handler->display->display_options['fields']['quandl_numeric_1']['quandl_result_key'] = 'Open';
$handler->display->display_options['fields']['quandl_numeric_1']['set_precision'] = TRUE;
$handler->display->display_options['fields']['quandl_numeric_1']['precision'] = '2';
$handler->display->display_options['fields']['quandl_numeric_1']['separator'] = '';
/* Field: Quandl: Numeric */
$handler->display->display_options['fields']['quandl_numeric_2']['id'] = 'quandl_numeric_2';
$handler->display->display_options['fields']['quandl_numeric_2']['table'] = 'quandl_views';
$handler->display->display_options['fields']['quandl_numeric_2']['field'] = 'quandl_numeric';
$handler->display->display_options['fields']['quandl_numeric_2']['label'] = 'High';
$handler->display->display_options['fields']['quandl_numeric_2']['quandl_result_key'] = 'High';
$handler->display->display_options['fields']['quandl_numeric_2']['set_precision'] = TRUE;
$handler->display->display_options['fields']['quandl_numeric_2']['precision'] = '2';
$handler->display->display_options['fields']['quandl_numeric_2']['separator'] = '';
/* Field: Quandl: Numeric */
$handler->display->display_options['fields']['quandl_numeric_3']['id'] = 'quandl_numeric_3';
$handler->display->display_options['fields']['quandl_numeric_3']['table'] = 'quandl_views';
$handler->display->display_options['fields']['quandl_numeric_3']['field'] = 'quandl_numeric';
$handler->display->display_options['fields']['quandl_numeric_3']['label'] = 'Low';
$handler->display->display_options['fields']['quandl_numeric_3']['quandl_result_key'] = 'Low';
$handler->display->display_options['fields']['quandl_numeric_3']['set_precision'] = TRUE;
$handler->display->display_options['fields']['quandl_numeric_3']['precision'] = '2';
$handler->display->display_options['fields']['quandl_numeric_3']['separator'] = '';
/* Field: Quandl: Numeric */
$handler->display->display_options['fields']['quandl_numeric']['id'] = 'quandl_numeric';
$handler->display->display_options['fields']['quandl_numeric']['table'] = 'quandl_views';
$handler->display->display_options['fields']['quandl_numeric']['field'] = 'quandl_numeric';
$handler->display->display_options['fields']['quandl_numeric']['label'] = 'Close';
$handler->display->display_options['fields']['quandl_numeric']['quandl_result_key'] = 'Close';
$handler->display->display_options['fields']['quandl_numeric']['set_precision'] = TRUE;
$handler->display->display_options['fields']['quandl_numeric']['precision'] = '2';
$handler->display->display_options['fields']['quandl_numeric']['separator'] = '';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'examples/quandl/stockprices';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'AAPL OHLC';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

$views[$view->name] = $view;
  
  
  $view = new view();
$view->name = 'quandl_test';
$view->description = 'Example showing basic usage of Quandl Views';
$view->tag = 'Quandl';
$view->base_table = 'quandl_views';
$view->human_name = 'Quandl Test';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Quandl Test';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['collapse'] = 'weekly';
$handler->display->display_options['query']['options']['use_args'] = '1';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '15';
$handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
$handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Items';
$handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '5, 10, 20, 40, 50, 100, 200';
$handler->display->display_options['style_plugin'] = 'views_bootstrap_grid_plugin_style';
$handler->display->display_options['style_options']['columns'] = '2';
$handler->display->display_options['row_plugin'] = 'fields';
/* Footer: Quandl: Metadata */
$handler->display->display_options['footer']['quandl_summary_area']['id'] = 'quandl_summary_area';
$handler->display->display_options['footer']['quandl_summary_area']['table'] = 'quandl_views';
$handler->display->display_options['footer']['quandl_summary_area']['field'] = 'quandl_summary_area';
$handler->display->display_options['footer']['quandl_summary_area']['content'] = 'Source: @source_name (@source_code)<br />Data set: @name (@code)<br />Last URL: @last_url<br />Column Names: @column_names<br />Frequency: @frequency Sort Order: @sort_order Premium: @premium';
/* Footer: Quandl: Download Links */
$handler->display->display_options['footer']['quandl_links_area']['id'] = 'quandl_links_area';
$handler->display->display_options['footer']['quandl_links_area']['table'] = 'quandl_views';
$handler->display->display_options['footer']['quandl_links_area']['field'] = 'quandl_links_area';
$handler->display->display_options['footer']['quandl_links_area']['content'] = '<br />Source: @quandl_call  - Download : @quandl_json_data : @quandl_xml_data : @quandl_csv_data';
/* No results behavior: Global: Messages */
$handler->display->display_options['empty']['messages']['id'] = 'messages';
$handler->display->display_options['empty']['messages']['table'] = 'views';
$handler->display->display_options['empty']['messages']['field'] = 'messages';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'No data returned.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Field: Quandl: Date */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'quandl_views';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['quandl_result_key'] = 'Date';
$handler->display->display_options['fields']['title']['date_format'] = 'short';
$handler->display->display_options['fields']['title']['second_date_format'] = 'long';
/* Field: Quandl: Generic */
$handler->display->display_options['fields']['custom']['id'] = 'custom';
$handler->display->display_options['fields']['custom']['table'] = 'quandl_views';
$handler->display->display_options['fields']['custom']['field'] = 'custom';
$handler->display->display_options['fields']['custom']['label'] = 'Open';
$handler->display->display_options['fields']['custom']['quandl_result_key'] = 'Open';
/* Field: Quandl: Generic */
$handler->display->display_options['fields']['custom_1']['id'] = 'custom_1';
$handler->display->display_options['fields']['custom_1']['table'] = 'quandl_views';
$handler->display->display_options['fields']['custom_1']['field'] = 'custom';
$handler->display->display_options['fields']['custom_1']['label'] = 'High';
$handler->display->display_options['fields']['custom_1']['quandl_result_key'] = 'High';
/* Field: Quandl: Generic */
$handler->display->display_options['fields']['custom_2']['id'] = 'custom_2';
$handler->display->display_options['fields']['custom_2']['table'] = 'quandl_views';
$handler->display->display_options['fields']['custom_2']['field'] = 'custom';
$handler->display->display_options['fields']['custom_2']['label'] = 'Low';
$handler->display->display_options['fields']['custom_2']['quandl_result_key'] = 'Low';
/* Field: Quandl: Generic */
$handler->display->display_options['fields']['custom_3']['id'] = 'custom_3';
$handler->display->display_options['fields']['custom_3']['table'] = 'quandl_views';
$handler->display->display_options['fields']['custom_3']['field'] = 'custom';
$handler->display->display_options['fields']['custom_3']['label'] = 'Close';
$handler->display->display_options['fields']['custom_3']['quandl_result_key'] = 'Close';
/* Field: Quandl: Generic */
$handler->display->display_options['fields']['custom_4']['id'] = 'custom_4';
$handler->display->display_options['fields']['custom_4']['table'] = 'quandl_views';
$handler->display->display_options['fields']['custom_4']['field'] = 'custom';
$handler->display->display_options['fields']['custom_4']['label'] = 'Volume';
$handler->display->display_options['fields']['custom_4']['quandl_result_key'] = 'Volume';
/* Field: Quandl: Numeric */
$handler->display->display_options['fields']['quandl_numeric']['id'] = 'quandl_numeric';
$handler->display->display_options['fields']['quandl_numeric']['table'] = 'quandl_views';
$handler->display->display_options['fields']['quandl_numeric']['field'] = 'quandl_numeric';
$handler->display->display_options['fields']['quandl_numeric']['label'] = 'Close';
$handler->display->display_options['fields']['quandl_numeric']['quandl_result_key'] = 'Close';
$handler->display->display_options['fields']['quandl_numeric']['set_precision'] = TRUE;
$handler->display->display_options['fields']['quandl_numeric']['precision'] = '4';
$handler->display->display_options['fields']['quandl_numeric']['separator'] = '';
/* Field: Quandl: Date */
$handler->display->display_options['fields']['quandl_date']['id'] = 'quandl_date';
$handler->display->display_options['fields']['quandl_date']['table'] = 'quandl_views';
$handler->display->display_options['fields']['quandl_date']['field'] = 'quandl_date';
$handler->display->display_options['fields']['quandl_date']['label'] = 'Age';
$handler->display->display_options['fields']['quandl_date']['quandl_result_key'] = 'Date';
$handler->display->display_options['fields']['quandl_date']['date_format'] = 'time ago';
$handler->display->display_options['fields']['quandl_date']['second_date_format'] = 'long';
/* Contextual filter: Global: Null */
$handler->display->display_options['arguments']['null']['id'] = 'null';
$handler->display->display_options['arguments']['null']['table'] = 'views';
$handler->display->display_options['arguments']['null']['field'] = 'null';
$handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Quandl Test - Table';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'custom' => 'custom',
  'custom_1' => 'custom_1',
  'custom_2' => 'custom_2',
  'custom_3' => 'custom_3',
  'custom_4' => 'custom_4',
  'quandl_numeric' => 'quandl_numeric',
  'quandl_date' => 'quandl_date',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'custom' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'custom_1' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'custom_2' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'custom_3' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'custom_4' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'quandl_numeric' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'quandl_date' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['path'] = 'examples/quandl/views/table';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'Quandl Views Table';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'Views Table';
$handler->display->display_options['tab_options']['weight'] = '0';

/* Display: Unformatted */
$handler = $view->new_display('page', 'Unformatted', 'page_1');
$handler->display->display_options['path'] = 'examples/quandl/views/unformatted';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Unformatted';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

 
$views[$view->name] = $view; 
  
  
  return $views;
}