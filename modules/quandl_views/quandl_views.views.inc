<?php

/**
 * @file
 * Definition of quandl_views_views.
 */

/**
 * Implementation of hook_views_plugins().
 */
function quandl_views_views_plugins() {
  $plugin = array();
  $plugin['query']['quandl_views_plugin_query'] = array(
    'title' => t('Quandl Query'),
    'help' => t('Returns data from Quandl queries.'),
    'handler' => 'quandl_views_plugin_query',
    
  );
  
  return $plugin;
}

/**
 * Implementation of hook_views_data().
 */
function quandl_views_views_data() {
  $data = array();

  // Base data
  $data['quandl_views']['table']['group']  = t('Quandl');
  $data['quandl_views']['table']['base'] = array(
    'title' => t('Quandl data'),
    'help' => t('Quandl rows from dataset.'),
    'query class' => 'quandl_views_plugin_query'
  );

  $data['quandl_views']['table']['quandl_source'] = array(
    'title' => t('Quandl sources'),
    'help' => t('Quandl datasets from a source.'),
    'query class' => 'quandl_views_plugin_query_list'
  );



    // Fields
    $data['quandl_views']['title'] = array(
        'title' => t('Date'),
        'help' => t('The Quandl date of this record.'),
        'field' => array(
          'handler' => 'quandl_views_handler_field_date',
        ),
    );
    
    // Custom / Text / Anything
    $data['quandl_views']['custom'] = array(
        'title' => t('Generic'),
        'help' => t('Generic column from the Quandl results.'),
        'field' => array(
          'handler' => 'quandl_views_custom_field',
        ),
    );
    
    // Numeric
    $data['quandl_views']['quandl_numeric'] = array(
        'title' => t('Numeric'),
        'help' => t('A numeric column from the Quandl results.'),
        'float' => TRUE,
        'field' => array(
          'handler' => 'quandl_views_numeric_field',
        ),
    );
    
    // Date
    $data['quandl_views']['quandl_date'] = array(
        'title' => t('Date'),
        'help' => t('A date column from the Quandl results.'),
        'field' => array(
          'handler' => 'quandl_views_handler_field_date',
        ),
    );

  // Footer / Header Handlers
  
  // Metadata
  $data['quandl_views']['quandl_summary_area'] = array(
    'title' => t('Metadata'),
    'help' => t('Show metadata for the data set, source and returned data. Useful for debugging and development.'),
    'area' => array(
      'handler' => 'quandl_views_handler_area_result',
    ),
  );
  
  
  // Download Links
  $data['quandl_views']['quandl_links_area'] = array(
    'title' => t('Download Links'),
    'help' => t('Show links for downloading the data set directly from Quandl servers.'),
    'area' => array(
      'handler' => 'quandl_views_handler_area_links',
    ),
  );
  
   
  // Filter String
  $data['quandl_views']['quandl_string']['filter'] = array(
    'title' => t('String'),
    'help' => t('Filter by strings.'),
    'area' => array(
      'handler' => 'quandl_views_handler_filter_string',
    ),
  );
 
  // Filter Date
  $data['quandl_views']['quandl_date']['filter'] = array(
    'title' => t('Date'),
    'help' => t('Filter by strings.'),
    'area' => array(
      'handler' => 'quandl_views_handler_filter_date',
    ),
  );


  // Filter Boolean
  $data['quandl_views']['quandl_numeric']['filter'] = array(
    'title' => t('Numeric'),
    'help' => t('Filter by Numeric.'),
    'area' => array(
      'handler' => 'quandl_views_handler_filter_numeric',
    ),
  );
 

  
  
  return $data;
}

