<?php

/**
 * @file
 *   Views field handler for Generic Quandl Fields.
 */

/**
 * Views field handler for Quandl.
 */
class quandl_views_custom_field extends views_handler_field {

  /**
   * Called to add the field to a query.
   */
  function query() {
    $this->field_alias = $this->real_field;
  }


  function option_definition() {
    $options = parent::option_definition();
    $options['quandl_result_key'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    // Offer a list of image styles for the user to choose from.
    parent::options_form($form, $form_state);

      $form['quandl_result_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Result Key'),
      '#default_value' => $this->options['quandl_result_key'],
      '#description' => t('The key for the column in the Quandl results. Example; Open, Close, High, Low.'),
    );   
    
    }

  /**
   * Render the field.
   *
   * @param $values
   *   The values retrieved from the database.
   */
  function render($values) {
    $val = (array) $values;
    $value = $val[$this->options['quandl_result_key']];  
    return $value;  
  }

}
