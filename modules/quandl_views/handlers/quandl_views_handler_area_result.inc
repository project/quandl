<?php

/**
 * @file
 * Definition of quandl_views_handler_area_result.
 */

/**
 * Views area handler to display some configurable result summary.
 *
 * @ingroup views_area_handlers
 */
class quandl_views_handler_area_result extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();

    $options['content'] = array(
      'default' => 'Quandl Source: @source_name (@source_code) - Data set: @name (@code)<br />Last URL: <em>@last_url</em>',
      'translatable' => TRUE,
    );

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $variables = array(
      'items' => array(
        '@source_name -- the human-readable name of source',
        '@source_code -- the Quandl code for the source',
        '@name -- the human-readable name of dataset',
        '@code -- the Quandl code for the dataset',
        '@updated_at -- timestamp of last update to data set',
        '@from_date -- start date of data set',
        '@to_date -- end date of data set',
        '@last_url -- last url requested from Quandl',
        '@column_names -- Column names for data returned',
        '@sort_order -- Sort order returned',
        '@frequency -- Frequency of each data point',
        '@premium -- true if this a premium dataset',
      ),
    );
    $list = theme('item_list', $variables);
    $form['content'] = array(
      '#title' => t('Display'),
      '#type' => 'textarea',
      '#rows' => 3,
      '#default_value' => $this->options['content'],
      '#description' => t('You may use HTML code in this field. The following tokens are supported: @list', array('@list' => $list)),
      
    );
  }


  /**
   * Find out the information to render.
   */
  function render($empty = FALSE) {
    
 
    
    // Must have options and does not work on summaries.
    if (!isset($this->options['content']) || $this->view->plugin_name == 'default_summary') {
      return;
    }
    $output = '';
    $format = $this->options['content'];
    
    $query_data = (array) $this->query->quandl_data;
    
    $source_code = $query_data['source_code'];
    $source_name = $query_data['source_name'];
    $code = $query_data['code'];
    $name = $query_data['name'];
    
    
    $updated_at = $query_data['updated_at'];
    $frequency = $query_data['frequency'];
    $from_date = $query_data['from_date'];
    $to_date = $query_data['to_date'];
    
    $last_url = $this->view->query->quandl_object->last_url;  
    
    $column_names = implode(' | ', $query_data['column_names']);
    $sort_order = $this->query->options['sort_order'];
    $frequency = $this->query->options['collapse'];
    $premium = var_export($query_data['premium'], TRUE);
    
    // Get the search information.
    $items = array('source_name', 'source_code', 'code', 'name', 'updated_at', 'from_date', 'to_date', 'last_url', 'column_names', 'sort_order', 'frequency', 'premium');
    $replacements = array();
    foreach ($items as $item) {
      $replacements["@$item"] = ${$item};
    }
    
      $output .= filter_xss_admin(str_replace(array_keys($replacements), array_values($replacements), $format));
 
 
    return $output;
  }
}
