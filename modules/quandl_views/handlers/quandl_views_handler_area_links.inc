<?php

/**
 * @file
 * Definition of quandl_views_handler_area_links.
 */

/**
 * Views area handler to display links to the Quandl data sets and sources.
 *
 * @ingroup views_area_handlers
 */
class quandl_views_handler_area_links extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();

    $options['content'] = array(
      'default' => 'Links: @quandl_call <br />JSON: @quandl_json_data <br />XML: @quandl_xml_data',
      'translatable' => TRUE,
    );

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $variables = array(
      'items' => array(
        '@quandl_call -- the Quandl URL for this data set',
        '@quandl_json_data -- the data from this call in JSON format',
        '@quandl_xml_data -- the data from this call in XML format',
        '@quandl_csv_data -- the data from this call in CSV format',
    ),
    );
    $list = theme('item_list', $variables);
    $form['content'] = array(
      '#title' => t('Display'),
      '#type' => 'textarea',
      '#rows' => 3,
      '#default_value' => $this->options['content'],
       '#description' => t('You may use HTML code in this field. The following tokens are supported: @list', array('@list' => $list)),
      
    );
  }


  /**
   * Find out the information to render.
   */
  function render($empty = FALSE) {

    // Must have options and does not work on summaries.
    if (!isset($this->options['content']) || $this->view->plugin_name == 'default_summary') {
      return;
    }
    $output = '';
    $format = $this->options['content'];
    
    $query_data = (array) $this->query->quandl_data;
    

    $source_code = $query_data['source_code'];
    $source_name = $query_data['source_name'];
    $dataset_code = $query_data['code'];
    $name = $query_data['name'];
    
    
    $original_url = $this->view->query->quandl_object->last_url;
    
    $quandl_call = t('<a href="@url">Quandl</a>', 
                        array('@url' => url('https://quandl.com/' . $source_code . '/' . $dataset_code )));
    
    $quandl_json_data = t('<a href="@url">JSON data</a>', 
                        array('@url' => url($original_url)));
    

    $xml_url = str_replace('.json', '.xml', $original_url);
                        
   $quandl_xml_data = t('<a href="@url">XML data</a>', 
                        array('@url' => url($xml_url)));

    $csv_url = str_replace('.json', '.csv', $original_url);
    
   $quandl_csv_data = t('<a href="@url">CSV data</a>', 
                        array('@url' => url($csv_url)));


    // Get the search information.
    $items = array('quandl_call', 'quandl_json_data', 'quandl_xml_data', 'quandl_csv_data');
    $replacements = array();
    foreach ($items as $item) {
      $replacements["@$item"] = ${$item};
    }
    // Send the output.

      $output .= filter_xss_admin(str_replace(array_keys($replacements), array_values($replacements), $format));

    return $output;
  }
}
