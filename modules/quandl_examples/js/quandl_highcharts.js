/* gpo-hc.js

Provides Highcharts support for GPO small and medium charts


*/


// Default GPO HC Settings object

jQuery(document).ready(function ($) {

$(function() {


    Highcharts.setOptions({
        chart: {
            backgroundColor: {
                linearGradient: [0, 0, 500, 500],
                stops: [
                    [0, 'rgb(255, 255, 255)'],
                    [1, 'rgba(243,210,19,1)']
                    ]
            },
            borderWidth: 0,
            plotBackgroundColor: 'rgba(255, 255, 255, .9)',
            plotShadow: false,
            plotBorderWidth: 1,
            type: 'ohlc',
        },
        
        credits: {
		      enabled: true,
		      text: 'quandrupal.com',
		      href: 'http://quandrupal.com',
		      color: '#fff',
		      position: {
					align: 'right',
					x: -10,
					verticalAlign: 'bottom',
					y: -5
					},
			style: {
				cursor: 'pointer',
				color: 'red',
				fontSize: '12px'
			
			}
		  },
  
		title: {
			    text: '',
			  	},        
 
 		subtitle: {
			    text: '',
			  	},        

 
        
   });

});

});




jQuery(document).ready(function ($) {


$(function () {
	    
	
	//console.log(Drupal.settings.quandl);
	
	var theData = Drupal.settings.chartSettings.data;
	var theVolume = Drupal.settings.chartSettings.volume;
	var columnNames = Drupal.settings.chartSettings.columnNames;
	var strings = Drupal.settings.chartSettings.strings;
	var overrides = Drupal.settings.chartSettings.overrides;
	    
    $('#container1').highcharts('StockChart', {
		
		strings: {
			currency: 'USD',
			instrument: 'XAU',
			closePrice: '1876.55',
			chartDate: 'Oct 15, 2014',
			chartTime: '21:55',
			chartZone: 'NY',
			metalType: strings.datasetName,
			percentChange: '5.32%',
			valueChange: '23.44',
			direction: '+',
		},
		
		chart: {
			
			
        	marginTop: 20,
        	marginBottom: 15,
        	borderColor: 'rgba(0, 0, 0, 0.3)',
            borderWidth: 1,
            borderRadius: 3,
			plotShadow: false,
			type: 'ohlc',
		},
		
		title: {
			    text: overrides.title,
			}, 
			
		subtitle: {
			    text: overrides.subtitle,
			}, 		
		plotOptions: {
            ohlc: {
                lineColor: 'green',
                upLineColor: 'green', // docs
                upColor: 'blue'
            }
        },
		
		
		xAxis: {
		    gridLineWidth: 1,
		    

		},
		
		yAxis: [
		
		{
		    gridLineWidth: 1,
		    opposite: false,
		    minorTickInterval: 50,
		    title: {
                    text: 'Price (USD)'
                },
            height: '70%',
		    plotLines: [{
                value: 0,
                width: 1,
                color: 'blue'
            }],
		},
		
		{
                labels: {
                    align: 'right',
                    x: -3
                },
                title: {
                    text: 'Volume (M)'
                },
                top: '75%',
                height: '25%',
                offset: 0,
               // lineWidth: 2
            },
		
		
		
		
		],
		
		rangeSelector : {
			enabled : true,
            selected: 1,
            },
		navigator : {enabled : true},
	    scrollbar : {enabled : false},
	    legend : {enabled : false},

        series: [{
        	name: 'AAPL ',
            data: theData,
            type: 'ohlc',
            
            tooltip: {
	            crosshairs: [true, true],
			},
    	} , 
        
                   
        {
        	type: 'column',
			name: 'Volume',
			data: theVolume,
			yAxis: 1,
            lineWidth: 2,
    	},
        
        
        ]

    }, function (chart) {
    
					// console.log(Drupal.settings.chartSettings.overrides);
					// Top Summary String
					
					var topText = '<span style="color: red">' + chart.options.strings.metalType  + '</span> ' 
						+ strings.summaryInfo;
					
			        chart.renderer.text(topText , 8, 16)
			            .css({color: '#000000',fontSize: '11px'})
			            .add();
						
					// Bottom Summary String
			        chart.renderer.text(strings.updatedAt, 8, chart.chartHeight - 5)
			            .css({color: '#000000',fontSize: '11px'})
			            .add();
						
			    }
    
    
    ); 

    $('#container2').highcharts('StockChart', {
		

		
		chart: {
			
        //	marginTop: 20,
        //	marginBottom: 15,
        	borderColor: 'rgba(0, 0, 0, 0.3)',
            borderWidth: 1,
            borderRadius: 5,
			plotShadow: false,
			type: 'line',
            plotBackgroundColor: 'white',
            
            backgroundColor: {
                linearGradient: [0, 0, 500, 500],
                stops: [
                    [0, 'rgb(255, 255, 255)'],
                    [1, 'rgba(0, 0, 0, 0.3)']
                    ]
            },

            
            
		},
		
		title: {
			    text: overrides.title,
			}, 
			
		subtitle: {
			    text: overrides.subtitle,
			},
		
		
		xAxis: {
		    gridLineWidth: 1,
		    

		},
		
		yAxis: [{
		    gridLineWidth: 1,
		    opposite: false,
		    minorTickInterval: 50,
		    title: {text: 'Price (USD)'},
		
		},		
		
		],
		
		rangeSelector : {
			enabled : true,
            },
		navigator : {enabled : true},
	    scrollbar : {enabled : true},
	    legend : {enabled : true},

        series: [
        
        {
        	name: 'Copper',
            data: theData.copper,
            type: 'line',
            tooltip: {crosshairs: [true, true],},
            
    	} , 

        {
        	name: 'Cobalt',
            data: theData.cobalt,
            type: 'line',
            tooltip: {crosshairs: [true, true],},
            
    	} , 


        {
        	name: 'Nickel',
            data: theData.nickel,
            type: 'line',
            tooltip: {crosshairs: [true, true],},
            
    	} ,         
  
        {
        	name: 'Tin',
            data: theData.tin,
            type: 'line',
            tooltip: {crosshairs: [true, true],},
            
    	} , 
    	
        {
        	name: 'Lead',
            data: theData.lead,
            type: 'line',
            tooltip: {crosshairs: [true, true],},
            
    	} ,    
    	
        {
        	name: 'Zinc',
            data: theData.zinc,
            type: 'line',
            tooltip: {crosshairs: [true, true],},
            
    	} , 	      
        ]

    }, function (chart) {
    
					console.log(Drupal.settings.chartSettings.overrides);
					// Top Summary String
					
					var topText = '<span style="color: red">' + 'Base Metals'  + '</span> ' 
						+ strings.summaryInfo;
					
			        chart.renderer.text(topText , 8, 16)
			            .css({color: '#000000',fontSize: '11px'})
			            .add();
						
					// Bottom Summary String
			        chart.renderer.text(strings.updatedAt, 8, chart.chartHeight - 5)
			            .css({color: '#000000',fontSize: '11px'})
			            .add();
						
			    }
    
    
    ); 

	        
	        
	  //  });
	
	});
	
	
	
});	
	
	
	
	