<?php

/**
 * Constructs a simple Quandl Charts page.
 *
 */
function quandl_multi_chart_page($arg1 = NULL) {
		
	
	$quandl = quandl_object();
	
	$params = array (
		'trim_start' => 'yesterday -2year',
		'trim_end' => 'yesterday',
		'sort_order' => 'asc',
		'exclude_data' => 'false',
		'collapse' => 'daily',
		//'reset_cache' => 'true',
	);
	
	$symbols = array (); //OFDP.COPPER_6,OFDP.COBALT_51,OFDP.NICKEL_41,OFDP.TIN_36
	
	$symbols[] = 'OFDP/COPPER_6.2';
	$symbols[] = 'OFDP/COBALT_51.2';
	$symbols[] = 'OFDP/NICKEL_41.2';
	$symbols[] = 'OFDP/TIN_36.2';
	$symbols[] = 'OFDP/LEAD_31.2';
	$symbols[] = 'OFDP/ZINC_26.2';
	
	$data = cached_quandl_multi_symbol_query($quandl, $symbols, $params) ;
	
	
//	dsm($quandl, 'Quandl object');
//	dsm($data, 'Quandl multi data object');
	
	$cobalt = array();
	$copper = array();
	$nickel = array();
	$tin = array();
	$lead = array();
	$zinc = array();
//	$vol_array = array();
	
	foreach($data->data as $key => $value) {
		$date = strtotime($value[0]) * 1000;
		$copper[] = array (
				$date,
				$value[1],  // Copper
				);
		$cobalt[] = array (
				$date,
				$value[2],	// Cobalt
				);		
		$nickel[] = array (
				$date,
				$value[3],  // Copper
				);
		$tin[] = array (
				$date,
				$value[4],	// Tin
				);		
		$lead[] = array (
				$date,
				$value[5],	// Lead
				);
	
		$zinc[] = array (
				$date,
				$value[6],	// Lead
				);
		
	}
	
	$prep_data = array (
		'copper' => $copper,
		'cobalt' => $cobalt,
		'nickel' => $nickel,
		'tin' => $tin,
		'lead' => $lead,
		'zinc' => $zinc,
		
	);
	
	//dsm($munged, 'Prepared Data');
	
	
	
	
	drupal_add_js('https://code.highcharts.com/stock/highstock.js', 
					array(
					  'type' => 'external',
					  'scope' => 'header',
					  'group' => JS_LIBRARY,
					  'every_page' => FALSE,
					  'weight' => -1,
					)
					);
					
	$path = drupal_get_path('module', 'quandl_examples') . '/js/quandl_highcharts.js';
	
	drupal_add_js($path, 
					array(
					  'type' => 'file',
					  'scope' => 'header',
					  'group' => JS_DEFAULT,
					  'every_page' => FALSE,
					  'weight' => -1,
					)
					);		 


	$strings_array = array (
	   // 'updatedAt' => t('Last Update - ') . $data->updated_at,
	    'datasetName' => 'Apple Computer (AAPL)',
	    'summaryInfo' => 'OHLC data',
    );				
					
    

	$charts_overrides = array (
		'title' => '5 Year Base Metal Asking Prices',
		'subtitle' => '',
    );				

	$chart_settings = array (
	    'data' => $prep_data,
	  //  'volume' => $vol_array,
	    'columnNames' => $data->column_names,
	    'strings' => $strings_array,
	    'overrides' => $charts_overrides,
	    
    );

	drupal_add_js(array('chartSettings' => $chart_settings), 'setting');


	$chart_markup = '<p>' . '<div id="container2" style="width:100%; height:600px;"></div>' . '</p>';
	$render = array();
	
	$description = '<p>Previous 5 years of various base metals.</p>';
	
	$render['description'] = array( '#type' => 'markup', '#markup' => $description,);
	
	$render['theChart'] = array( '#type' => 'markup', '#markup' => $chart_markup,);

	return $render;
}




/**
 * Constructs a simple Quandl Charts page.
 *
 */
function quandl_chart_page($arg1 = NULL) {
	
	// https://www.quandl.com/api/v1/datasets/WIKI/AAPL.json?trim_start=2009-11-24&trim_end=2014-11-24&sort_order=desc&exclude_data=false&collapse=weekly&auth_token=fDvDXQg7xWzLd1rGAKHE
	
	
	$quandl = quandl_object();
	
	$params = array (
		'trim_start' => 'yesterday -5 years',
		'trim_end' => 'yesterday',
		'sort_order' => 'asc',
		'exclude_data' => 'false',
		'collapse' => 'daily',
	);
	
	$symbol = 'WIKI/AAPL';
	
	$data = cached_quandl_symbol_query($quandl, $symbol, $params) ;
	
	
//	dsm($quandl, 'Quandl object');
//	dsm($data, 'Quandl data object');
//	
	$munged = array();
	$vol_array = array();
	
	foreach($data->data as $key => $value) {
		
		$munged[] = array (
				strtotime($value[0]) * 1000, // JS time
				$value[1],  // Open
				$value[2],	// High
				$value[3],	// Low
				$value[4],	// Close
				);
		
		$vol_array[] = array (
				strtotime($value[0]) * 1000,
				$value[5],	// Volume
				);
		
	}
	
	
	
	
	drupal_add_js('https://code.highcharts.com/stock/highstock.js', 
					array(
					  'type' => 'external',
					  'scope' => 'header',
					  'group' => JS_LIBRARY,
					  'every_page' => FALSE,
					  'weight' => -1,
					)
					);
					
	$path = drupal_get_path('module', 'quandl_examples') . '/js/quandl_highcharts.js';
	
	drupal_add_js($path, 
					array(
					  'type' => 'file',
					  'scope' => 'header',
					  'group' => JS_DEFAULT,
					  'every_page' => FALSE,
					  'weight' => -1,
					)
					);		 


	$strings_array = array (
	    'updatedAt' => t('Last Update - ') . $data->updated_at,
	    'datasetName' => 'Apple Computer (AAPL)',
	    'summaryInfo' => 'OHLC data',
    );				
					
    

	$charts_overrides = array (
		'title' => 'Apple Inc.',
		'subtitle' => 'OHLC Data',
    );				

	$chart_settings = array (
	    'data' => $munged,
	    'volume' => $vol_array,
	    'columnNames' => $data->column_names,
	    'strings' => $strings_array,
	    'overrides' => $charts_overrides,
	    
    );

	drupal_add_js(array('chartSettings' => $chart_settings), 'setting');


	$chart_markup = '<p>' . '<div id="container1" style="width:100%; height:600px;"></div>' . '</p>';
	$render = array();
	
	$description = '<p>Previous 5 years Open-High-Low-Close (OHLC) plus Volumes chart for Apple Inc.</p>';
	
	$render['description'] = array( '#type' => 'markup', '#markup' => $description,);
	
	$render['theChart'] = array( '#type' => 'markup', '#markup' => $chart_markup,);

	return $render;
}


