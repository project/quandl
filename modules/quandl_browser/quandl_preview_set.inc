<?php


/**
 * @file quandl_preview_set.inc
 * Constructs a simple Quandl Preview page of rows from a Dataset.
 *
 */
function quandl_preview_set_page($search_term = NULL, $dataset) {
  
    
    $target_source = 'AUD';
    if (!empty($search_term)) {
        $target_source = $search_term;  
    } 
   
    $qoptions = array();  

    $render = array();
    $render['theList'] = drupal_get_form('quandl_preview_set_form', $target_source, $dataset);


    return $render;
}


function build_preview_set_table_headers($results) {
    $headers = array();
    
    foreach ($results->column_names as $key => $value) {
        
        $headers[] = array('data' => t($value));
        
    }
    
    return $headers;
    
}


/**
 * Callback
 * Constructs a simple Quandl Dataset Preview page.
 *
 */

function quandl_preview_set_callback($form, $form_state) {
  
    
    $search_term = $form_state['values']['search_term'];
    $source_code = $form_state['values']['source_code'];
    
    $quandl = quandl_object();
    

    $page = 1;
    $page_items = 100;


    
    $symbol = $source_code . '/' . $search_term;
    
    
    $params = array();
    $params['sort_order']    = 'asc';
    $params['exclude_data'] = 'false';
    $params['collapse'] = 'none';
    
    
    $search_data = cached_quandl_symbol_query($quandl, $symbol, $params);
  
  
  $element = $form['preview_set_results'];
  $table_data = quandl_preview_set_table_build($search_data);
  $element['#markup'] = drupal_render($table_data);
  return $element;
}


function quandl_preview_set_form($form, $form_state, $term, $dataset) { 
    
  $form['quandl_preview_set'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preview Datasets'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['quandl_preview_set']['search_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Browse Quandl Datasets - @term', array('@term' => check_plain($term))),
    '#required' => FALSE,
    '#default_value' => $dataset,
    '#description' => t("Please enter a Data Code to list the datasets for your Source."),
    '#size' => 40,
    '#maxlength' => 255,
  );

  $form['quandl_preview_set']['source_code'] = array(
    '#type' => 'hidden',
    '#required' => FALSE,
    '#default_value' => $term,
  );

  
  $form['quandl_preview_set']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Datasets',
    '#ajax' => array(
      'callback' => 'quandl_preview_set_callback',
      'wrapper' => 'preview_set_results',
    ),


   $form['preview_set_results'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="preview_set_results">',
    '#suffix' => '</div>',
  ),
   
  );

  return $form;
}





/**
 * Build the table render array.
 *
 * @return array
 *   A render array set for theming by theme_table().
 */
function quandl_preview_set_table_build($results) {
    

    $series_data = $results->data;

    if (!$series_data) {
        $build = array(
              '#markup' => '<div>No results were found for that Source Code. Please try another</div>',
            );

        return $build;
        
    }



  $rows = array();
  foreach ($series_data as $row) {
    
    // Make it an array
    $arr = (array) $row;
        
    $rows[] = array('data' => $arr);
    
  }
  
  
  // Build the table for the nice output.
  $build = array(
    '#markup' => '<div>The following records were found.</div>',
    '#theme' => 'table',
    '#header' => build_preview_set_table_headers($results),
    '#rows' => $rows,
  );

  return $build;
}
