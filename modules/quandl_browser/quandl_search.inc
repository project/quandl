<?php

/**
 *
 * @file quandl_search.inc
 *
 * Constructs a simple Quandl Search page.
 *
 */

/**
 * Constructs a simple Quandl Search page.
 *
 */
function quandl_search_page($search_term) {
    
    
    $target_source = 'Bitcoin';
    if (!empty($search_term)) {
       $target_source = $search_term;   
    } 

    
    $results = array();
    
    $render = array();
    
    
    $headers = array();
    $headers[] = array('data' => t('ID'));
    $headers[] = array('data' => t('Code'));
    $headers[] = array('data' => t('Data Sets'));
    $headers[] = array('data' => t('Description'));
    $headers[] = array('data' => t('Name'));
    $headers[] = array('data' => t('Host'));
    $headers[] = array('data' => t('Premium'));
    $headers[] = array('data' => t('Concurrency'));
    
    
    $render['theSearch'] = drupal_get_form('quandl_search_form', $target_source);


    return $render;
}



function quandl_search_form($form, &$form_state, $term) {


  $form['quandl'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Quandl Datasets'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['quandl']['search_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Search Quandl Sources and Datasets for'),
    '#required' => FALSE,
    '#default_value' => $term,
    '#description' => t("Please enter keywords for the datasets you are looking for."),
    '#size' => 40,
    '#maxlength' => 255,
  );
  $form['quandl']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Search',
    '#ajax' => array(
      'callback' => 'quandl_search_callback',
      'wrapper' => 'search_results',
    ),

   $form['search_results'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="search_results">',
    '#suffix' => '</div>',
  ),
   
  );
  
  return $form;
}



function quandl_search_callback($form, $form_state) {
 
    $start_time = microtime(TRUE);
    
    $search_term = $form_state['values']['search_term'];
    
    $quandl = quandl_object();
    
    $page = 1;
    $page_items = 300;
    
    $search_data = cached_quandl_search($quandl, $search_term, $page, $page_items);
    
    $results = $search_data->sources;

    $end_time = microtime(TRUE);
    $duration = $end_time - $start_time;
    
    
    
    $element = $form['search_results'];
    $table_array = quandl_search_table_build( $results);
    $element['#markup'] = drupal_render($table_array);
    return $element;
}



/**
 * Build the table render array.
 *
 * @return array
 *   A render array set for theming by theme_table().
 */
function quandl_search_table_build($series_data) {
    

    $headers = array();
    $headers[] = array('data' => t('Code'));
    $headers[] = array('data' => t('Data Sets'));
    $headers[] = array('data' => t('Description'));
    $headers[] = array('data' => t('Name'));
    $headers[] = array('data' => t('Premium'));




  $rows = array();
  foreach ($series_data as $row) {
    
    $row->code = '<a href="/quandl/sources/list/' . $row->code . '">' . $row->code . '</a>';
    $row->premium = var_export($row->premium, TRUE);
    
    // Make it an array
    $arr = (array) $row;

    
    $host = l($arr['host'], 'http://' . $arr['host']);
    
    $arr['name'] = $arr['name'] . '<br />' . $host;
    
    unset($arr['id']); 
    unset($arr['host']); 
    unset($arr['subscribed']); 
    
    
    $rows[] = array('data' => $arr);
  }

  // Build the table for the nice output.
  $build = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $rows,
    '#prefix' => '<div id="result-table">',
    '#suffix' => '</div>',
    '#attributes' => array(
                        'class' => array(
                                            'table-bordered',
                                            'table-hover',
                                            'table-condensed')
                        ),

  );

  return $build;
}
