<?php


/**
 * Constructs a simple Quandl Favourites page.
 *
 */
function quandl_favourites_page($arg1 = NULL) {
    
    $quandl = quandl_object();
        
    $list_data = $quandl->getFavourites();

  //  dsm($list_data);

    
     // return array('#markup' => '<pre>' . $print_data . '</pre>');
     
//  $chart_markup = '<p>' . '<div id="highcharts-container" style="width:100%; height:400px;"></div>' . '</p>';
    $render = array();
//  $render['theChart'] = array( '#type' => 'markup', '#markup' => $chart_markup,);
    
    $headers = array ();
    $headers[] = array('data' => t('Source'));
    $headers[] = array('data' => t('Code'));
    $headers[] = array('data' => t('Dataset'));
    $headers[] = array('data' => t('Name'));
    //$headers[] = array('data' => t('URL'));
    //$headers[] = array('data' => t('Description'));
    $headers[] = array('data' => t('Frequency'));
    $headers[] = array('data' => t('Start'));
    $headers[] = array('data' => t('End'));
    $headers[] = array('data' => t('Columns'));
    
    
//  $render['theSeach'] = quadl_search_form();
    $render['theTable'] = quandl_list_table_build($headers, $list_data->docs);
//  $render['theTable']['#caption'] = 'I am a caption';
//  $render = $render +test_test ();
    //dsm($render, 'Render');
    return $render;
}



/**
 * Build the table render array.
 *
 * @return array
 *   A render array set for theming by theme_table().
 */
function quandl_list_table_build($header_data, $series_data) {

/*

    $columns = array();
    foreach ($header_data as $col) {
        $columns[] = array('data' => t($col));
    }
*/
 // dsm(gettype($series_data));
  $rows = array();
  foreach ($series_data as $row) {
    
    // Make it an array
    $arr = (array) $row;
    
    // remove some fields we dont need for this table
    unset($arr['id']); 
    // unset($arr['source_code']); 
    unset($arr['urlize_name']); 
    unset($arr['description']); 
    unset($arr['display_url']); 
    unset($arr['created_at']); 
    unset($arr['updated_at']); 
    unset($arr['private']); 
    unset($arr['type']); 
    //unset($arr['premium']); 
    
    $arr['column_names'] = '<ul><li>' . implode('<li>', $arr['column_names']) . '</ul>';
    
    
    $rows[] = array('data' => $arr);
    
  }
  
  
  // Build the table for the nice output.
  $build = array(
    '#theme' => 'table',
    '#header' => $header_data,
    '#rows' => $rows,
  );

  return $build;
}
