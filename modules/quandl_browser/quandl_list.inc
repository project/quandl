<?php


/**
 * @file quandl_list.inc
 * Provides a simple list of of datasets for a Quandl Source.
 *
 *
 *
 */


/**
 * Constructs a simple Quandl Search page.
 *
 */
function quandl_list_page($search_term = NULL) {
    
    
    $target_source = 'WIKI';
    if (!empty($search_term)) {
       $target_source = $search_term;   
    } 

    $render = array();

    $render['theList'] = drupal_get_form('quandl_list_form', $target_source);
    
    return $render;
}


function build_list_table_headers() {
    $headers = array();
    
    $headers[] = array('data' => t('Code'));
    $headers[] = array('data' => t('Name'));
    $headers[] = array('data' => t('Frequency'));
    $headers[] = array('data' => t('Start'));
    $headers[] = array('data' => t('End'));
    $headers[] = array('data' => t('Premium'));
    
    return $headers;
    
}


function quandl_list_callback($form, $form_state) {
  
    
    $search_term = $form_state['values']['search_term'];
    
    $quandl = quandl_object();
    

    $page = 1;
    $page_items = 300;


    $search_data = cached_quandl_list_query($quandl, $search_term, $page, $page_items);

    $results = $search_data->docs;
  
  
  $element = $form['list_results'];
  $table_data = quandl_list_table_build($results);
  $element['#markup'] = drupal_render($table_data);
  
  return $element;
}


function quandl_list_form($form, $form_state, $term) { 
    
  $form['quandl_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Quandl Sources'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['quandl_list']['search_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Search Quandl Sources'),
    '#required' => FALSE,
    '#default_value' => $term,
    '#description' => t("Please enter a Quandl Code to list the datasets."),
    '#size' => 40,
    '#maxlength' => 255,
  );
  $form['quandl_list']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Search',
    '#ajax' => array(
      'callback' => 'quandl_list_callback',
      'wrapper' => 'list_results',
    ),


   $form['list_results'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="list_results">',
    '#suffix' => '</div>',
  ),
   
  );

  return $form;
}





/**
 * Build the table render array.
 *
 * @return array
 *   A render array set for theming by theme_table().
 */
function quandl_list_table_build($series_data) {


    if (!$series_data) {
        $build = array(
            '#markup' => '<div>No results were found for that Source Code</div>',
          );

        return $build;
        
    }


  $rows = array();
  foreach ($series_data as $row) {
    
    // Make it an array
    $arr = (array) $row;
    
    // remove some fields we dont need for this table
 
    unset($arr['urlize_name']); 
    unset($arr['description']); 
    unset($arr['display_url']); 
    unset($arr['created_at']); 
    unset($arr['updated_at']); 
    unset($arr['private']); 
    unset($arr['type']); 
    
    $quandl_code = $arr['source_code'] . '/' . $arr['code'];
    
    $arr['name'] = $arr['name'] . '<br /><small>' . $arr['host'] . '</small>';
    $arr['code'] = '<a href="/quandl/sources/preview_set/' . $quandl_code . '">' . $arr['code'] . '</a><br /><small>' . $quandl_code . '</small>';
    
    
    $arr['premium'] = var_export($arr['premium'], TRUE);
    
    unset($arr['id']); 
    unset($arr['source_id']);
    unset($arr['source_code']); 
    
    $rows[] = array('data' => $arr);
    
  }
  
  
  // Build the table for the nice output.
  $build = array(
    '#markup' => '<div>The following datasets were found.</div>',
    '#theme' => 'table',
    '#header' => build_list_table_headers(),
    '#rows' => $rows,
  );

  return $build;
}
